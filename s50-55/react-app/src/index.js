import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
// import bootstrap
import "bootstrap/dist/css/bootstrap.min.css";

// createRoot - assigns the elements to be managed by React with its virtual DOM
// render() - displays the react element/componets
// app component is our mother component, this is the component we use as an entry ponit and where we can render all other components or pages

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name = "John Miller";
// const user = {
//   firstName: "Jane",
//   lastName: "Doe"
// };

// function formatName(user) {
//   return user.firstName + " " + user.lastName;
// }

// const element = <h1>Hello, {name}</h1>;

// // library          retain or container
// const root = ReactDOM.createRoot(document.getElementById("root"));

// root.render(element);
