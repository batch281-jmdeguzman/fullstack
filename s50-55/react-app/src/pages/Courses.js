import { Fragment, useEffect, useState } from "react";
import CourseCard from "../components/CourseCard"
// import courseData from "../data/courseData"; old database

export default function Courses() {
  // Check to see if the mock data was captured
  // console.log(courseData);

  // the "map" method loops through the individual course objects in our array and returns a componenet for each caourse
  // multiple componenes created through the map method must have unique key taht will help react js  indentify which cmponenet5elemets have been changes
  // everytime the map method loops through the data, it creates a "courseCard" componenet and then passess the current element in our courseData array using the courseProp
  const [courses, setCourses] = useState([]);
 //we are goint to add an useEffect here so that in every time that we refresj our application it will fetch the updated content of our courses.
  useEffect(()=>{
    // defines here weather the courses if active or not by putting the url endpoint
    fetch(`${process.env.REACT_APP_API_URL}/courses/`)
    .then(response => response.json())
    .then(data => {
      setCourses(data.map(course => {
        return(
          <CourseCard key = {courses._id} courseProp = {
            course}/>
        )
      }))
      // console.log(data);
    })

  },[])

  // const courses = courseData.map(course=>{
  //   return(
  //     <CourseCard key={course.id} courseProp={course}/>

  //   )
  // })
  // The Course in the coursecard component is called a prop which is a shorthand for "property" since cmponents are consideered as objects in ReactJS
  // the curly braces ({}) are use for props to sigify that we are providing information using javascript expression ratherr that hard coded values which use double qoutes""
  // We can pass information from one component to anoter using props this is prefered to as props drilling
  return (
    <Fragment>
      {courses}
    </Fragment>
  )
}
