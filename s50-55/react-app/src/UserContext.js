import React from 'react';

// create a context object
// A context object as the name states is a data type of an object that can be used to store information that can be share shared within the app
// the context object is a different approached to passing information between componenet and allows easier access avoidng the use of props

// using the create context from react we can create a context in our app
// we contain the context created in our usercontext variable
// we name it usercontext simply because this context will contain the information of our user.

const UserContext = React.createContext();

// the provider component allows other component to consume/use the context object and supply the necessary information needed to the context object.

export const UserProvider = UserContext.Provider;

export default UserContext;