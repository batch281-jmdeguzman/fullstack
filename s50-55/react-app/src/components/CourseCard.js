import { Button, Card, Row, Col } from "react-bootstrap";
import PropTypes from 'prop-types';
import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}) {
  // check to see if the data was successfilly passed
  console.log(courseProp)

  const {_id,name,description,price} = courseProp

  // use the state hook for this component to be able to store its state
  // .....


    const [isDisabled, setIsDisabled] = useState(false);


  const [count, setCount]=useState(0)
  const [seats, setSeats] = useState(30)

     //we are goinbg to create a new state that will declare or tell the value of the disabled property in the button.

    // function that keeps track of the enrollees
    // the stter function for UseState are asynchronus allowng it to execute separately from other codes in the program
    // the "setCount" function is being excuted while the console log is already completed
  // function enroll(){
  //   setCount(count+1);
  //   console.log('Enrolees:'+count)
  // }

  function enroll(){
    if(seats>1){
    setCount(count+1)
    setSeats(seats-1)
    // console.log('Enrolees:'+count)
    // console.log('Enrolees:'+seats)
    }else{
      alert('Congratulations on getting last slot!')
      setSeats(seats-1);
    }
  }


  // define 'useEffect' hook to have "CourseCard" component do or perform a centain task after every changes inthe seats states
  // the side effect will runautomatically in initial rendereing and in every changes of the seats states
  // the array and the useEffect is called the defendency array


  useEffect(() =>{
    if(seats === 0){
      setIsDisabled(true);

    }

  }, [seats]);

  return (
    <Card>
        <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>{price}</Card.Text>
            <Card.Subtitle>Enrollees</Card.Subtitle>
            <Card.Text>{count} Enrollees</Card.Text>
            <Card.Subtitle>Seats</Card.Subtitle>
            <Card.Text>{seats}</Card.Text>
            {/*<Button variant="primary" onClick ={enroll} disabled={isDisabled}>Enroll</Button>*/}
             <Button as= {Link} to ={`/courses/${_id}`} variant="primary" disabled={isDisabled}>See more details</Button>
        </Card.Body>
    </Card>
  )
}
// check if the CourseCard component is getting the correct prop types
// promotes are used for validating information passed to a component and is a tooll normally use to help develeopers ensure the correct inforrmation is being passed from one component to the next 

CourseCard.propTypes = {
  // The shape method is used to check if a prop object conforms to a specific shape
  courseProp: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
};